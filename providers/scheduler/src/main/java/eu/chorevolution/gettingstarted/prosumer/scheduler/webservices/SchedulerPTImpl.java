package eu.chorevolution.gettingstarted.prosumer.scheduler.webservices;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import eu.chorevolution.gettingstarted.provider.scheduler.ScheduleDetails;
import eu.chorevolution.gettingstarted.provider.scheduler.ScheduleRequest;
import eu.chorevolution.gettingstarted.provider.scheduler.ScheduleResponse;
import eu.chorevolution.gettingstarted.provider.scheduler.SchedulerPT;

@Component(value="SchedulerPTImpl")
public class SchedulerPTImpl implements SchedulerPT{

	private static Logger LOGGER = LoggerFactory.getLogger(SchedulerPTImpl.class);

	@Override
	public ScheduleResponse organizeSchedule(ScheduleRequest parameters) {
		
		LOGGER.info("*******SCHEDULER*******");
		LOGGER.info("* Organizing Schedule *");
		ScheduleResponse scheduleResponse = new ScheduleResponse();
		scheduleResponse.setOrderID(parameters.getOrderID());
		ScheduleDetails scheduleDetails = new ScheduleDetails();
		scheduleDetails.setAddress(parameters.getAddress());
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		scheduleDetails.setDepartureDate(today);
		calendar.add(Calendar.DATE, 3);
		scheduleDetails.setArrivalDate(calendar.getTime());
		scheduleResponse.setScheduleDetails(scheduleDetails);
		LOGGER.info("* Order ID "+ scheduleResponse.getOrderID() +" *");
		LOGGER.info("* Address: "+scheduleResponse.getScheduleDetails().getAddress().getCity()+", "+scheduleResponse.getScheduleDetails().getAddress().getStreet()+", "+scheduleResponse.getScheduleDetails().getAddress().getPostcode()+" *");			
		LOGGER.info("* Departure Date "+ scheduleResponse.getScheduleDetails().getDepartureDate() +" *");
		LOGGER.info("* Arrival Date "+ scheduleResponse.getScheduleDetails().getArrivalDate() +" *");
		LOGGER.info("*******SCHEDULER*******");
		return scheduleResponse;
	}
	


}
