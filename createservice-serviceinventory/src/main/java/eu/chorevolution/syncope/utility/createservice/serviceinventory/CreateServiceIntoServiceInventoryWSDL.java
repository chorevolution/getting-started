/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.syncope.utility.createservice.serviceinventory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.cxf.helpers.IOUtils;
import org.apache.syncope.common.lib.to.AnyObjectTO;
import org.apache.syncope.common.lib.to.AttrTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.InterfaceDescriptionType;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.Service;
import eu.chorevolution.syncope.utility.createservice.serviceinventory.model.ServiceRole;

public class CreateServiceIntoServiceInventoryWSDL {
	
	private static final Logger LOG = LoggerFactory.getLogger(CreateServiceIntoServiceInventoryWSDL.class);

	public static String URL = "http://localhost:9080/syncope/rest/";
	public static String USERNAME = "admin";
	public static String PASSWORD = "password";
	public static String DOMAIN = "Master";

	private static final String INPUT_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "main"
			+ File.separatorChar + "resources" + File.separatorChar;

	//private static final String INPUT_RESOURCES = "." + File.separatorChar;
	private static final String INTERFACE_INPUT_RESOURCES = INPUT_RESOURCES + "interface" + File.separatorChar;

	private static ApacheSyncopeUtilities syncopeUtilities;

	public static void main(String[] args) throws FileNotFoundException, IOException {
		syncopeUtilities = new ApacheSyncopeUtilities(URL, USERNAME, PASSWORD, DOMAIN);
		LOG.info("adding provider services");
		addProviderServices();
	}

	private static void addProviderServices() {
		Service service = null;
		try {
			LOG.info("adding carrier service");
			service = new Service("carrier", "http://chorevolution.disim.univaq.it/carrier/carrier/",
					
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "carrier.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("carrier_role", ""));
			syncopeUtilities.createService(service);
			

			LOG.info("adding invoicer service");
			service = new Service("invoicer", "http://chorevolution.disim.univaq.it/invoicer/invoicer/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "invoicer.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("invoicer_role", ""));
			syncopeUtilities.createService(service);

			LOG.info("adding paymentsystem service");
			service = new Service("paymentsystem", "http://chorevolution.disim.univaq.it/paymentsystem/paymentsystem/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "paymentsystem.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("paymentsystem_role", ""));
			syncopeUtilities.createService(service);

			LOG.info("adding scheduler service");
			service = new Service("scheduler", "http://chorevolution.disim.univaq.it/scheduler/scheduler/",
					IOUtils.readBytesFromStream(new FileInputStream(INTERFACE_INPUT_RESOURCES + "scheduler.wsdl")),
					InterfaceDescriptionType.WSDL);
			service.addServiceRole(createRole("scheduler_role", ""));
			syncopeUtilities.createService(service);

		} catch (IOException e) {
			LOG.error("While loading provider services", e);
		}
	}


	private static ServiceRole createRole(String roleName, String description) {
		AnyObjectTO role = syncopeUtilities.createRole(roleName, description);

		ServiceRole serviceRole = new ServiceRole();

		serviceRole.setKey(role.getKey());
		serviceRole.setName(role.getName());

		Iterator<AttrTO> iterator = role.getPlainAttrs().iterator();
		while (iterator.hasNext()) {

			AttrTO attr = iterator.next();
			if (attr.getSchema().equalsIgnoreCase(ApacheSyncopeUtilities.SERVICE_ROLE_DESCRIPTION)) {
				serviceRole.setDescription(attr.getValues().get(0));
			}

		}

		return serviceRole;

	}

}
